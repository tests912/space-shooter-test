﻿using System;
using Zenject;
using UniRx;


namespace SpaceShooter.UI
{
    public class UIPresenter : Disposable, IUIPresenter, IInitializable
    {
        #region Fields
        private readonly SignalBus _signalBus;
        private readonly UIView _uiView;
        private readonly UIModel _uiModel;
        #endregion

        #region Constructors
        public UIPresenter(UIView uiView, UIModel uiModel, SignalBus signalBus)
        {
            _signalBus = signalBus;
            _uiView = uiView;
            _uiModel = uiModel;
        }
        #endregion

        #region Methods
        public void Initialize()
        {
            _signalBus.GetStream<MapSelectSignal>().Subscribe(signal => ShowInGameUI()).AddTo(GetDisposableSubscriptions);
            _signalBus.GetStream<MapShowSignal>().Subscribe(_ => ShowMapUI()).AddTo(GetDisposableSubscriptions);

            _uiView.backButton.OnClickAsObservable().Subscribe(_ => _signalBus.Fire<MapShowSignal>());
            _uiView.resetButton.OnClickAsObservable().Subscribe(_ => _signalBus.Fire<MapResetSignal>());
        }


        public void ShowMapUI()
        {
            HideInGameUI();
            _uiView.mapUIGroup.SetActive(true);
        }

        public void ShowInGameUI()
        {
            HideMapUI();
            _uiView.inGameUIGroup.SetActive(true);

        }

        public void HideMapUI()
        {
            _uiView.mapUIGroup.SetActive(false);
        }

        public void HideInGameUI()
        {
            _uiView.inGameUIGroup.SetActive(false);
        }

        public void SetLivesText(string lives)
        {
            _uiView.livesText.text = lives;
        }

        public void SetProgressText(string progress)
        {
            _uiView.progressText.text = progress;
        }

        public void ShowLevelComleate()
        {
            _uiView.backButton.interactable = false;
            _uiView.eventText.text = _uiModel.levelCompleateText;
            _uiView.eventText.gameObject.SetActive(true);
            Observable.Timer(TimeSpan.FromMilliseconds(2500)).Subscribe(_ =>
            {
                _uiView.eventText.gameObject.SetActive(false);
                _uiView.backButton.interactable = true;
            });
        }

        public void ShowGameOver()
        {
            _uiView.backButton.interactable = false;
            _uiView.eventText.text = _uiModel.gameOverText;
            _uiView.eventText.gameObject.SetActive(true);
            Observable.Timer(TimeSpan.FromMilliseconds(2500)).Subscribe(_ =>
            {
                _uiView.eventText.gameObject.SetActive(false);
                _uiView.backButton.interactable = true;
            });
        }
        #endregion
    }
}