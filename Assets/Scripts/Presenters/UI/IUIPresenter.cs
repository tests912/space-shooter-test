﻿
namespace SpaceShooter.UI
{
    public interface IUIPresenter
    {
        #region Methods
        void ShowMapUI();
        void HideMapUI();
        void ShowInGameUI();
        void HideInGameUI();

        void SetLivesText(string lives);
        void SetProgressText(string progress);
        void ShowLevelComleate();
        void ShowGameOver();
        #endregion
    }
}