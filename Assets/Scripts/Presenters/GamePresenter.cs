﻿using System;
using UnityEngine;
using Zenject;
using UniRx;
using SpaceShooter.Ship;
using SpaceShooter.Map;

namespace SpaceShooter.Game
{
    public class GamePresenter : Disposable, IInitializable
    {
        #region Fields
        private readonly FactoryIShipPresenter _shipFactory;
        private readonly SignalBus _signalBus;
        private readonly LevelHelper _levelHelper;
        private readonly IMapModel _mapModel;

        private IShipPresenter _ship;
        private int _currentPointModel;
        #endregion

        #region Constructors
        public GamePresenter(FactoryIShipPresenter factory, IMapModel mapModel, LevelHelper levelHelper, SignalBus signalBus)
        {
            _shipFactory = factory;
            _signalBus = signalBus;
            _levelHelper = levelHelper;
            _currentPointModel = 0;
            _mapModel = mapModel;
        }
        #endregion

        #region Methods
        public void Initialize()
        {
            _signalBus.GetStream<MapSelectSignal>().Subscribe(x => StartGame(x.mapIndex)).AddTo(GetAditionalDisposableSubscriptions);
            _signalBus.GetStream<MapShowSignal>().Subscribe(x => StopGame()).AddTo(GetAditionalDisposableSubscriptions);
            _signalBus.GetStream<ShipCrashSignal>().Subscribe(x => ResetGame()).AddTo(GetAditionalDisposableSubscriptions);
        }

        public void StartGame(int indexMapPoint)
        {
            _currentPointModel = indexMapPoint;
            _ship = _shipFactory.Create(_mapModel.Points[_currentPointModel]);
        }


        public void StopGame()
        {
            GameObject.Destroy(((MonoBehaviour)_ship).gameObject);
        }

        public void ResetGame()
        {
            Observable.Timer(TimeSpan.FromMilliseconds(2500)).Subscribe(_ =>
            {
                _mapModel.Points[_currentPointModel].ShipModel.ResetShip();
            }).AddTo(GetDisposableSubscriptions);
        }
        #endregion
    }
}