﻿using SpaceShooter.Map;
using System.Collections;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;


namespace SpaceShooter.Ship.Shells
{
    public partial class BulletPresenter : DisposableBehaviour, IShellPresenter
    {
        #region Fields
        private IMapPointModel _mapPointModel;
        private LevelHelper _levelHelper;
        [SerializeField]
        private BulletView _view;
        #endregion

        #region Props
        [SerializeField] IShellModel _model;
        public IShellModel GetModel => _model;
        #endregion

        #region Public methods
        [Inject]
        public void Construct(LevelHelper levelHelper, IMapPointModel mapPointModel, IShellModel model)
        {
            _levelHelper = levelHelper;
            _model = model;
            _view.rigidbody2d = _view.rigidbody2d ?? GetComponent<Rigidbody2D>();
            _view.renderer = _view.renderer ?? GetComponent<SpriteRenderer>();
            _view.collision2DTrigger = _view.collision2DTrigger ?? GetComponent<ObservableCollision2DTrigger>();
            _mapPointModel = mapPointModel;
            transform.position = _model.InGameCoords.Value;
        }

        private void Start()
        {
            _view.rigidbody2d.position = _model.InGameCoords.Value;
            _view.rigidbody2d.ObserveEveryValueChanged(rg => rg.position).ToReactiveProperty().Subscribe(cords => _model.InGameCoords.SetValueAndForceNotify(cords))
                .AddTo(GetDisposableSubscriptions);
            _model.GetInGameVelocity.Subscribe(velocity => _view.rigidbody2d.velocity = velocity).AddTo(GetDisposableSubscriptions);


            _view.collision2DTrigger.OnCollisionEnter2DAsObservable().Subscribe(_ => DestroyShell()).AddTo(GetDisposableSubscriptions);
            Observable.FromMicroCoroutine(xc => CheckForDestroy(), false, FrameCountType.Update).Subscribe(_ => DestroyShell()).AddTo(GetDisposableSubscriptions);
        }

        public void DestroyShell()
        {
            _mapPointModel.ShipModel.GetBullets.Remove((BulletModel)_model);
            Destroy(gameObject);
        }
        #endregion

        #region Private methods
        private IEnumerator CheckForDestroy()
        {
            while (!_levelHelper.CheckForOutOfScreen(transform.position, _view.rigidbody2d.velocity, 2).isOut)
            {
                yield return null;
            }
        }
        #endregion
    }
}