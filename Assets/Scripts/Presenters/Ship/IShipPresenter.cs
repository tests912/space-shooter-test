﻿
namespace SpaceShooter.Ship
{
    public interface IShipPresenter
    {
        void Fire();
    }
}