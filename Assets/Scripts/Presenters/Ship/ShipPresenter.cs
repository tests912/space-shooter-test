﻿using SpaceShooter.Map;
using SpaceShooter.Ship.Shells;
using SpaceShooter.UI;
using System;
using System.Collections.Generic;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Zenject;


namespace SpaceShooter.Ship
{
    public partial class ShipPresenter : DisposableBehaviour, IShipPresenter
    {
        #region Fields
        private FactoryIShellPresenter _shelsFactory;
        private SignalBus _signalBus;
        private LevelHelper _levelHelper;
        private IUIPresenter _uIPresenter;
        private IMapPointModel _mapPointModel;

        private List<IShellPresenter> _shels;

        [SerializeField]
        private IShipModel _model;
        [SerializeField]
        private ShipView _view;
        #endregion

        #region Methods
        [Inject]
        public void Construct(LevelHelper levelHelper, IShipModel model, IMapPointModel mapPointModel, FactoryIShellPresenter shelsFactory, IUIPresenter uIPresenter, SignalBus signalBus)
        {
            _levelHelper = levelHelper;

            _model = mapPointModel.ShipModel = mapPointModel.ShipModel ?? (ShipModel)model;
            _mapPointModel = mapPointModel;
            _signalBus = signalBus;
            _shelsFactory = shelsFactory;
            _uIPresenter = uIPresenter;
            _shels = new List<IShellPresenter>();
            _view.renderer = _view.renderer ?? GetComponent<SpriteRenderer>();
            _view.collider = _view.collider ?? GetComponent<BoxCollider2D>();
            _view.collision2DTrigger = _view.collision2DTrigger ?? GetComponent<ObservableCollision2DTrigger>();
        }

        private void Start()
        {
            _signalBus.GetStream<MapShowSignal>().Subscribe(x => ClearBullets()).AddTo(GetAditionalDisposableSubscriptions);
            _signalBus.GetStream<LevelPassSignal>().Subscribe(x => LevelClear()).AddTo(GetAditionalDisposableSubscriptions);

            if (_model.GetBullets != null && _model.GetBullets.Count > 0)
                CreateBullets();


            transform.position = _model.InGameCoords.Value;
            transform.ObserveEveryValueChanged(t => t.position).ToReactiveProperty().Subscribe(cords => _model.InGameCoords.SetValueAndForceNotify(cords))
                .AddTo(GetDisposableSubscriptions);


            Observable.EveryFixedUpdate()
                .Select(_ => Input.GetAxis("Horizontal"))
                .Subscribe(axis => transform.Translate(Vector3.right * axis * _model.GetInputSpeed.Value)).AddTo(GetDisposableSubscriptions);

            Observable.EveryUpdate()
              .Where(_ => Input.GetButton(_model.GetFireButtonName.Value) && _model.InGameLives.curentLives.Value>0)
              .ThrottleFirst(TimeSpan.FromMilliseconds(_model.GetShotDelayMs.Value))
              .Subscribe(_ => Fire()).AddTo(GetDisposableSubscriptions);

            Observable.EveryUpdate().Subscribe(_ =>
            {
                var result = _levelHelper.CheckForOutOfScreen(transform.position, new Vector2(Input.GetAxis("Horizontal"), 0), 1);
                if (result.isOut)
                {
                    transform.position = result.mirrorPos;
                }
            }).AddTo(GetDisposableSubscriptions);

            _view.collision2DTrigger.OnCollisionEnter2DAsObservable().Subscribe(_ => _model.Collide()).AddTo(GetDisposableSubscriptions);

            _model.InGameLives.curentLives.Subscribe(l => _uIPresenter.SetLivesText(l.ToString())).AddTo(GetDisposableSubscriptions);
            _model.InGameLives.curentLives.Where(x => x < 1).Subscribe(_ => ShipCrash());
        }


        private void ShipCrash()
        {
            _signalBus.Fire<ShipCrashSignal>();
            _uIPresenter.ShowGameOver();
            Destroy(_view.collider);
            Destroy(_view.renderer);
            Observable.Timer(TimeSpan.FromMilliseconds(2500)).Subscribe(_ =>
            {
                _signalBus.Fire<MapShowSignal>();
                LevelClear();
            });


        }

        private void LevelClear()
        {
            ResetAllBullets();
            ClearBullets();
        }

        private void ResetAllBullets()
        {
            _model.GetBullets.Clear();
        }

        private void ClearBullets()
        {
            foreach (var shellPresenter in _shels)
            {
                try
                {
                    Destroy(((MonoBehaviour)shellPresenter).gameObject);
                }
                catch { }
            }

            _shels.Clear();
        }

        private void CreateBullets()
        {
            foreach (var shellModel in _model.GetBullets)
            {
                CreateBullet(shellModel);
            }
        }

        private void CreateBullet(IShellModel shellModel)
        {
            var _shellPresenter = _shelsFactory.Create(shellModel, _mapPointModel);

            _shels.Add(_shellPresenter);
        }

        public void Fire()
        {
            var _shellModel = _model.CreateBullet(new Vector2ReactiveProperty(transform.position));
            CreateBullet(_shellModel);
        }
        #endregion

    }
}