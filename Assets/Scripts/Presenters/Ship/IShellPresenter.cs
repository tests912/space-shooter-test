﻿
namespace SpaceShooter.Ship.Shells
{
    public interface IShellPresenter
    {
        void DestroyShell();
    }
}