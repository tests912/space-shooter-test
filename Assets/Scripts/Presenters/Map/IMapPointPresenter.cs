﻿
namespace SpaceShooter.Map
{
    public interface IMapPointPresenter
    {
        IMapPointModel GetModel { get; }
    }
}