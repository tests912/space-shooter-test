﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;


namespace SpaceShooter.Map
{
    [Serializable]
    public class MapPointPresenter : DisposableBehaviour, IMapPointPresenter
    {
        #region Fields
        private LevelHelper _levelHelper;
        private SignalBus _signalBus;

        [SerializeField]
        private IMapPointModel _model;
        [SerializeField]
        private MapPointView _view;
        #endregion

        #region Props
        public IMapPointModel GetModel => _model;
        #endregion

        #region Methods
        [Inject]
        public void Construct(LevelHelper levelHelper, IMapPointModel model, SignalBus signalBus)
        {
            _levelHelper = levelHelper;
            _model = model;
            _signalBus = signalBus;
            _view.image = _view.image ?? GetComponent<Image>();
            _view.button = _view.button ?? GetComponent<Button>();
            _view.text = _view.text ?? GetComponent<Text>();
            _view.rectTransform = _view.rectTransform ?? transform.GetChild(0).GetComponent<RectTransform>();
        }

        private void Start()
        {
            Observable.CombineLatest(_model.GetCoords, _levelHelper.GetMapSize).Subscribe(cords => _view.rectTransform.anchoredPosition = new Vector2(cords[1].x / 2 * cords[0].x, cords[1].y / 2 * cords[0].y));
            _model.GetText.Subscribe(text => _view.text.text = text).AddTo(GetDisposableSubscriptions);
            _model.Status.Subscribe(status =>
            {
                switch (status)
                {
                    case MapPointState.open:
                        _view.button.interactable = true;
                        _view.button.image.sprite = _view.missionOpen;
                        _view.text.color += new Color(0, 0, 0, 1);
                        break;
                    case MapPointState.closed:
                        _view.button.interactable = false;
                        _view.button.image.sprite = _view.missionOpen;
                        _view.text.color -= new Color(0, 0, 0, 0.5f);
                        break;
                    case MapPointState.compleate:
                        _view.button.interactable = true;
                        _view.button.image.sprite = _view.missionComleate;
                        _view.text.color += new Color(0, 0, 0, 1);
                        break;

                    default:
                        break;
                }


            });

            _view.button.OnClickAsObservable().Subscribe(_ => StartLevel()).AddTo(GetDisposableSubscriptions);
        }


        public void StartLevel()
        {
            if (_model.Status.Value != MapPointState.closed)
            {
                Debug.Log($"Start lvl {_model.GetIndex}");
                _signalBus.Fire(new MapSelectSignal() { mapIndex = _model.GetIndex });
            }
        }
        #endregion

    }
}