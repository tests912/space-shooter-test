﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;

namespace SpaceShooter.Map
{
    public class MapPresenter : Disposable, IInitializable
    {
        #region Fields
        private readonly IMapModel _model;
        private readonly FactoryIMapPointPresenter _mapPointFactory;
        private readonly SignalBus _signalBus;
        private readonly LevelHelper _levelHelper;

        private List<IMapPointPresenter> _mapPoints;
        #endregion

        #region Constructors
        public MapPresenter(FactoryIMapPointPresenter factory, IMapModel mapModel, LevelHelper levelHelper, SignalBus signalBus)
        {
            _model = mapModel;
            _mapPointFactory = factory;
            _signalBus = signalBus;
            _mapPoints = new List<IMapPointPresenter>();
        }
        #endregion

        #region Methods
        public void Initialize()
        {
            _signalBus.GetStream<MapResetSignal>().Subscribe(_ => Reset());
            if (_model.Points != null && _model.Points.Length > 0)
                CreateMapPoints();
            else
                Reset();
        }


        public void CreateMapPoints()
        {
            foreach (var point in _model.Points)
            {
                var mapPoint = _mapPointFactory.Create(point);
                _mapPoints.Add(mapPoint);
            }
        }

        public void Reset()
        {
            foreach (var item in _mapPoints)
            {
                GameObject.Destroy(((MonoBehaviour)item).gameObject);
            }
            _mapPoints.Clear();

            _model.Generate(UnityEngine.Random.Range(2, 5));

            CreateMapPoints();
        }

        public override void Dispose()
        {
            base.Dispose();

            _model.Save();
        }
        #endregion
    }
}