﻿
namespace SpaceShooter.Obstacles
{
    public interface IObstaclePresenter
    {
        #region Props
        IObstacleModel GetModel { get; }
        #endregion

        #region Methods
        void DestroyObstacle();
        #endregion
    }
}