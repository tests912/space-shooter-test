﻿using System;
using UnityEngine;
using Zenject;
using UniRx;
using System.Collections;
using SpaceShooter.Map;

namespace SpaceShooter.Obstacles
{
    [Serializable]
    public partial class AsteroidPresenter : DisposableBehaviour, IObstaclePresenter
    {
        #region Fields
        private IMapPointModel _mapPointModel;
        private LevelHelper _levelHelper;
        private SignalBus _signalBus;

        [SerializeField]
        private AsteroidView _view;
        [SerializeField]
        private IObstacleModel _model;
        #endregion

        #region Props
        public IObstacleModel GetModel => _model;
        #endregion

        #region Private methods
        [Inject]
        private void Construct(LevelHelper levelHelper, IMapPointModel mapPointModel, IObstacleModel model, SignalBus signalBus)
        {
            _levelHelper = levelHelper;
            _model = model;
            _signalBus = signalBus;
            _view.rigidbody2d = _view.rigidbody2d ?? GetComponent<Rigidbody2D>();
            _view.renderer = _view.renderer ?? GetComponent<SpriteRenderer>();
            _mapPointModel = mapPointModel;
        }

        private void Start()
        {
            _model.GetInGameColor.Subscribe(color => _view.renderer.color = color).AddTo(GetDisposableSubscriptions);
            _view.rigidbody2d.position = _model.InGameCoords.Value;
            _view.rigidbody2d.ObserveEveryValueChanged(rg => rg.position).ToReactiveProperty().Subscribe(cords => _model.InGameCoords.SetValueAndForceNotify(cords))
                .AddTo(GetDisposableSubscriptions);
            _model.GetInGameScale.Subscribe(scale => transform.localScale = scale).AddTo(GetDisposableSubscriptions);
            _model.GetInGameVelocity.Subscribe(velocity => _view.rigidbody2d.velocity = velocity).AddTo(GetDisposableSubscriptions);

            _view.collision2DTrigger.OnCollisionEnter2DAsObservable().Subscribe(_ => DestroyObstacle()).AddTo(GetDisposableSubscriptions);
            Observable.FromMicroCoroutine(xc => CheckForDestroy(), false, FrameCountType.Update).Subscribe(_ => DestroyObstacle()).AddTo(GetDisposableSubscriptions);
        }


        private IEnumerator CheckForDestroy()
        {
            while (!_levelHelper.CheckForOutOfScreen(transform.position, _view.rigidbody2d.velocity, 2).isOut)
            {
                yield return null;
            }
        }
        #endregion

        #region Public methods
        public void DestroyObstacle()
        {
            if (_mapPointModel.GetObstaclesModel.AllowSpawn.Value && _mapPointModel.ShipModel.InGameLives.curentLives.Value > 0)
                _mapPointModel.CurrentProgress.SetValueAndForceNotify(_mapPointModel.CurrentProgress.Value + 1);
            _mapPointModel.GetObstaclesModel.Obstacles.Remove((AsteroidModel)_model);
            Destroy(gameObject);
        }
        #endregion
    }
}