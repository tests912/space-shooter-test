﻿using SpaceShooter.Map;
using SpaceShooter.UI;
using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;


namespace SpaceShooter.Obstacles
{
    public class ObstaclesPresenter : Disposable, IInitializable
    {
        #region Fields
        private readonly IMapModel _mapModel;
        private readonly FactoryIObstaclePresenter _obstaclesFactory;
        private readonly SignalBus _signalBus;
        private readonly LevelHelper _levelHelper;
        private readonly IUIPresenter _uIPresenter;

        private List<IObstaclePresenter> _obstacles;
        private int _currentPointModel;
        #endregion

        #region Constructors
        public ObstaclesPresenter(FactoryIObstaclePresenter factory, IUIPresenter uIPresenter, ObstaclesModel model, IMapModel mapModel, LevelHelper levelHelper, SignalBus signalBus)
        {
            _mapModel = mapModel;
            _obstaclesFactory = factory;
            _signalBus = signalBus;
            _levelHelper = levelHelper;
            _obstacles = new List<IObstaclePresenter>();
            _currentPointModel = 0;
            _uIPresenter = uIPresenter;
        }
        #endregion

        #region Public methods
        public void Initialize()
        {
            _signalBus.GetStream<MapSelectSignal>().Subscribe(x => SetModelIndexAndStart(x.mapIndex)).AddTo(GetAditionalDisposableSubscriptions);
            _signalBus.GetStream<MapShowSignal>().Subscribe(x => Stop()).AddTo(GetAditionalDisposableSubscriptions);
            _signalBus.GetStream<ShipCrashSignal>().Subscribe(x => ShipCrash()).AddTo(GetAditionalDisposableSubscriptions);

        }

        public void LevelComleate()
        {
            _mapModel.Points[_currentPointModel].GetObstaclesModel.AllowSpawn.SetValueAndForceNotify(false);

            _mapModel.CompleateLvl(_currentPointModel);
            _mapModel.OpenNextLvl(_currentPointModel);
            _uIPresenter.ShowLevelComleate();
            Observable.Timer(TimeSpan.FromMilliseconds(2500)).Subscribe(_ =>
            {
                _signalBus.Fire<LevelPassSignal>();
                _signalBus.Fire<MapShowSignal>();
                ResetAllObstacles();
            }).AddTo(GetDisposableSubscriptions);
        }

        public void ShipCrash()
        {
            Observable.Timer(TimeSpan.FromMilliseconds(2500)).Subscribe(_ =>
            {
                ResetAllObstacles();
            }).AddTo(GetDisposableSubscriptions);
        }

        public void CreateObstacle()
        {
            var model = _mapModel.Points[_currentPointModel].GetObstaclesModel.CreateObstacle(
                GetSpawnPosition(),
                _mapModel.Points[_currentPointModel].GetScaleFactor.Value,
                _mapModel.Points[_currentPointModel].GetSpeedFactor.Value,
                _mapModel.Points[_currentPointModel].GetColor.Value);

            var obstacle = _obstaclesFactory.Create(model, _mapModel.Points[_currentPointModel]);


            _obstacles.Add(obstacle);

        }

        public void CreateObstacles()
        {
            foreach (var obstacleModel in _mapModel.Points[_currentPointModel].GetObstaclesModel.Obstacles)
            {
                var obstaclePresenter = _obstaclesFactory.Create(obstacleModel, _mapModel.Points[_currentPointModel]);
                _obstacles.Add(obstaclePresenter);
            }
        }

        public void ResetAllObstacles()
        {
            _mapModel.Points[_currentPointModel].GetObstaclesModel.Obstacles.Clear();
            _mapModel.Points[_currentPointModel].CurrentProgress.SetValueAndForceNotify(0);
        }

        public void ClearObstaclesPresenters()
        {
            foreach (var obstaclePresenter in _obstacles)
            {
                try
                {
                    GameObject.Destroy(((MonoBehaviour)obstaclePresenter).gameObject);
                }
                catch (Exception) { }
            }
            _obstacles.Clear();
        }

        public void SetModelIndexAndStart(int index)
        {
            _currentPointModel = index;

            _mapModel.Points[_currentPointModel].CurrentProgress.Subscribe(progress => _uIPresenter.SetProgressText($"{progress}/{_mapModel.Points[_currentPointModel].GetMaxScore}")).AddTo(GetDisposableSubscriptions);
            _mapModel.Points[_currentPointModel].GetObstaclesModel.AllowSpawn.SetValueAndForceNotify(true);

            if (_mapModel.Points[_currentPointModel].GetObstaclesModel.Obstacles != null && _mapModel.Points[_currentPointModel].GetObstaclesModel.Obstacles.Count > 0)
                CreateObstacles();


            Observable.EveryUpdate()
               .Where(_ => _mapModel.Points[_currentPointModel].GetObstaclesModel.AllowSpawn.Value)
               .ThrottleFirst(TimeSpan.FromMilliseconds(_mapModel.Points[_currentPointModel].GetObstaclesModel.GetSpawnDelay.Value))
               .Subscribe(_ => CreateObstacle()).AddTo(GetDisposableSubscriptions);

            _mapModel.Points[_currentPointModel].CurrentProgress.Where(x => x >= _mapModel.Points[_currentPointModel].GetMaxScore.Value).Subscribe(_ => LevelComleate()).AddTo(GetDisposableSubscriptions);
        }

        public void Stop()
        {
            GetDisposableSubscriptions.Clear();
            _mapModel.Points[_currentPointModel].GetObstaclesModel.AllowSpawn.SetValueAndForceNotify(false);
            _mapModel.Save();
            ClearObstaclesPresenters();
        }
        #endregion

        #region Private methods
        private Vector2 GetSpawnPosition()
        {
            return _levelHelper.GetMainCamera.ScreenToWorldPoint(new Vector3(UnityEngine.Random.Range(-100, Screen.width + 100), Screen.height + 100));
        }

        private float GetRandomXPosition()
        {
            return _levelHelper.GetMainCamera.ScreenToWorldPoint(new Vector3(UnityEngine.Random.Range(-100, Screen.width + 100), 0)).x;
        }
        #endregion
    }
}