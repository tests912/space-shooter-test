﻿using System;
using UnityEngine;


namespace SpaceShooter.UI
{
    [Serializable]
    public class UIModel
    {
        #region Fields
        [Header("In game texts")]
        public string levelCompleateText = "Level comleate!";
        public string gameOverText = "Game Over!";
        #endregion
    }
}