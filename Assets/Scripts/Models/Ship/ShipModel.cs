﻿using SpaceShooter.Ship.Shells;
using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;


namespace SpaceShooter.Ship
{
    [Serializable]
    public class ShipModel : IShipModel
    {
        #region Fields
        [SerializeField] private Vector2ReactiveProperty _inGameCoords;
        [SerializeField] private IntReactiveProperty _inGameLives;
        [SerializeField] private FloatReactiveProperty _getInputSpeed;
        [SerializeField] private IntReactiveProperty _getShotDelayMs;
        [SerializeField] private StringReactiveProperty _getFireButtonName;
        [SerializeField] private FloatReactiveProperty _bulletSpeed;
        [SerializeField] private List<BulletModel> _bullets;
        [SerializeField] private int _initialLives;
        #endregion

        #region Props
        public Vector2ReactiveProperty InGameCoords { get => _inGameCoords; set => _inGameCoords = value; }
        public (IntReactiveProperty, int) InGameLives { get => (_inGameLives, _initialLives); set => _inGameLives = value.Item1; }
        public FloatReactiveProperty GetInputSpeed => _getInputSpeed;
        public IntReactiveProperty GetShotDelayMs => _getShotDelayMs;
        public StringReactiveProperty GetFireButtonName => _getFireButtonName;
        public FloatReactiveProperty GetBulletSpeed => _bulletSpeed;
        public List<BulletModel> GetBullets => _bullets;
        #endregion

        #region Constructors
        public ShipModel()
        {
            _inGameCoords = new Vector2ReactiveProperty(new Vector2(0, -4.1f));
            _inGameLives = new IntReactiveProperty(3);
            _initialLives = 3;
            _getInputSpeed = new FloatReactiveProperty(1);
            _bulletSpeed = new FloatReactiveProperty(10);
            _getShotDelayMs = new IntReactiveProperty(150);
            _getFireButtonName = new StringReactiveProperty("Fire1");
            _bullets = new List<BulletModel>();
        }

        public ShipModel(Vector2ReactiveProperty inGameCoords, IntReactiveProperty inGameLives, FloatReactiveProperty getInputSpeed, IntReactiveProperty getShotDelayMs, StringReactiveProperty getFireButtonName, List<BulletModel> bullets)
        {
            _inGameCoords = inGameCoords;
            _inGameLives = inGameLives;
            _getInputSpeed = getInputSpeed;
            _getShotDelayMs = getShotDelayMs;
            _getFireButtonName = getFireButtonName;
            _bullets = bullets;
        }
        #endregion

        #region Methods
        public IShellModel CreateBullet(Vector2ReactiveProperty pos)
        {
            var shell = new BulletModel(pos, new Vector2ReactiveProperty(Vector2.up * _bulletSpeed.Value));
            _bullets.Add(shell);
            return shell;
        }

        public void Collide()
        {
            if (_inGameLives.Value > 0)
                _inGameLives.SetValueAndForceNotify(_inGameLives.Value - 1);
        }

        public void ResetShip()
        {
            _inGameLives = new IntReactiveProperty(_initialLives);
            _inGameCoords = new Vector2ReactiveProperty(new Vector2(0, _inGameCoords.Value.y));
        }
        #endregion
    }
}