﻿using System;
using UniRx;
using UnityEngine;


namespace SpaceShooter.Ship.Shells
{
    [Serializable]
    public class BulletModel : IShellModel
    {
        #region Fields
        [SerializeField] private Vector2ReactiveProperty _inGameCoords;
        [SerializeField] private Vector2ReactiveProperty _inGameVelocity;
        #endregion

        #region Props
        public Vector2ReactiveProperty InGameCoords { get => _inGameCoords; set => _inGameCoords = value; }
        public Vector2ReactiveProperty GetInGameVelocity => _inGameVelocity;
        #endregion

        #region Constructors
        public BulletModel()
        {
        }

        public BulletModel(Vector2ReactiveProperty inGameCoords, Vector2ReactiveProperty inGameVelocity)
        {
            _inGameCoords = inGameCoords;
            _inGameVelocity = inGameVelocity;
        }
        #endregion
    }

}