﻿using UniRx;

namespace SpaceShooter.Ship.Shells
{
    public interface IShellModel
    {
        #region Props
        Vector2ReactiveProperty InGameCoords { get; set; }
        Vector2ReactiveProperty GetInGameVelocity { get; }
        #endregion
    }
}