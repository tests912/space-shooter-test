﻿using SpaceShooter.Ship.Shells;
using System.Collections.Generic;
using UniRx;

namespace SpaceShooter.Ship
{
    public interface IShipModel
    {
        #region Props
        Vector2ReactiveProperty InGameCoords { get; set; }
        (IntReactiveProperty curentLives, int initialLives) InGameLives { get; set; }
        FloatReactiveProperty GetInputSpeed { get; }
        IntReactiveProperty GetShotDelayMs { get; }
        StringReactiveProperty GetFireButtonName { get; }
        FloatReactiveProperty GetBulletSpeed { get; }
        List<BulletModel> GetBullets { get; }
        #endregion

        #region Constructors
        IShellModel CreateBullet(Vector2ReactiveProperty inGameCoords);
        #endregion

        #region Methods
        void Collide();
        void ResetShip();
        #endregion
    }
}