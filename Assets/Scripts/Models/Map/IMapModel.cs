﻿
namespace SpaceShooter.Map
{
    public interface IMapModel
    {
        #region Props
        IMapPointModel[] Points { get; }
        #endregion

        #region Methods
        void Save();
        void Generate(int count);
        void CompleateLvl(int currentPointIndex);
        void OpenNextLvl(int currentPointIndex);
        #endregion
    }
}
