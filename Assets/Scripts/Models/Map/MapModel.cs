﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UniRx;
using SpaceShooter.Obstacles;
using SpaceShooter.Ship;

namespace SpaceShooter.Map
{
    [System.Serializable]
    public class MapModel : IMapModel
    {
        #region Fields
        private readonly ISerializer _serializer;
        private readonly string _savePath;

        [SerializeField]
        private MapPointModel[] _points;
        #endregion

        #region Props
        public IMapPointModel[] Points => _points;
        #endregion

        #region Constructors
        public MapModel(ISerializer serializer, string savePath)
        {
            _serializer = serializer;
            _savePath = savePath;
            _points = _serializer.Load<MapPointModel[]>(savePath);
        }
        #endregion

        #region Methods
        public void Save()
        {
            _serializer.Save(_points, _savePath);
        }

        public void Generate(int count)
        {
            _points = new MapPointModel[count];
            for (int i = 0; i < count; i++)
            {
                _points[i] = (MapPointModel)System.Activator.CreateInstance(typeof(MapPointModel),
                    new ReactiveProperty<MapPointState>(MapPointState.closed),
                    new Vector2ReactiveProperty(new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f))),
                    new StringReactiveProperty((i + 1).ToString()),
                    new FloatReactiveProperty(Random.Range(0.2f, 1f)),
                    new FloatReactiveProperty(Random.Range(1f, 15f)),
                    new ColorReactiveProperty(Random.ColorHSV()),
                    new ObstaclesModel(),
                    new ShipModel(),
                    new IntReactiveProperty(Random.Range(10, 50)),
                    new IntReactiveProperty(0),
                    i);
            }
            _points[0].Status = new ReactiveProperty<MapPointState>(MapPointState.open);

            Save();
        }

        public void CompleateLvl(int currentPointIndex)
        {
            if (Points.Length > currentPointIndex)
            {
                Points[currentPointIndex].Status.SetValueAndForceNotify(MapPointState.compleate);
            }
        }

        public void OpenNextLvl(int currentPointIndex)
        {
            if (Points.Length > currentPointIndex + 1)
            {
                Points[currentPointIndex + 1].Status.SetValueAndForceNotify(MapPointState.open);
            }
        }
        #endregion
    }
}