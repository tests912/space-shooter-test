﻿using SpaceShooter.Obstacles;
using SpaceShooter.Ship;
using UniRx;

namespace SpaceShooter.Map
{
    public interface IMapPointModel
    {
        #region Props
        int GetIndex { get; }
        ReactiveProperty<MapPointState> Status { get; set; }
        Vector2ReactiveProperty GetCoords { get; }
        StringReactiveProperty GetText { get; }
        FloatReactiveProperty GetScaleFactor { get; }
        FloatReactiveProperty GetSpeedFactor { get; }
        ColorReactiveProperty GetColor { get; }
        IntReactiveProperty GetMaxScore { get; }
        IntReactiveProperty CurrentProgress { get; set; }

        ObstaclesModel GetObstaclesModel { get; }
        IShipModel ShipModel { get; set; }
        #endregion
    }
}