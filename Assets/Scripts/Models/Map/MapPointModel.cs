﻿using SpaceShooter.Obstacles;
using SpaceShooter.Ship;
using System;
using UniRx;
using UnityEngine;

namespace SpaceShooter.Map
{
    [Serializable]
    public class MapPointModel : IMapPointModel
    {
        #region Fields
        [SerializeField] private ReactiveProperty<MapPointState> _status;
        [SerializeField] private Vector2ReactiveProperty _coords;
        [SerializeField] private StringReactiveProperty _text;
        [SerializeField] private FloatReactiveProperty _scaleFactor;
        [SerializeField] private FloatReactiveProperty _speedFactor;
        [SerializeField] private ColorReactiveProperty _color;
        [SerializeField] private ObstaclesModel _obstaclesModel;
        [SerializeField] private IShipModel _shipModel;
        [SerializeField] private IntReactiveProperty _maxScore;
        [SerializeField] private IntReactiveProperty _currentProgress;
        [SerializeField] private int _index;
        #endregion

        #region Props
        public ReactiveProperty<MapPointState> Status
        {
            get { return _status; }
            set { _status = value; }
        }
        public Vector2ReactiveProperty GetCoords => _coords;
        public StringReactiveProperty GetText => _text;
        public FloatReactiveProperty GetScaleFactor => _scaleFactor;
        public FloatReactiveProperty GetSpeedFactor => _speedFactor;
        public ColorReactiveProperty GetColor => _color;
        public ObstaclesModel GetObstaclesModel => _obstaclesModel;
        public IShipModel ShipModel { get => _shipModel; set => _shipModel = value; }
        public IntReactiveProperty GetMaxScore => _maxScore;
        public IntReactiveProperty CurrentProgress { get => _currentProgress; set => _currentProgress = value; }
        public int GetIndex => _index;
        #endregion

        #region Constructors
        public MapPointModel(ObstaclesModel obstaclesModel)
        {
            _obstaclesModel = obstaclesModel;
        }

        public MapPointModel(ReactiveProperty<MapPointState> status,
                             Vector2ReactiveProperty coords,
                             StringReactiveProperty text,
                             FloatReactiveProperty scaleFactor,
                             FloatReactiveProperty speedFactor,
                             ColorReactiveProperty color,
                             ObstaclesModel obstaclesModel,
                             IShipModel shipModel,
                             IntReactiveProperty maxScore,
                             IntReactiveProperty currentProgress,
                             int index)
        {
            _status = status;
            _coords = coords;
            _text = text;
            _scaleFactor = scaleFactor;
            _speedFactor = speedFactor;
            _color = color;
            _obstaclesModel = obstaclesModel;
            _shipModel = shipModel;
            _maxScore = maxScore;
            _currentProgress = currentProgress;
            _index = index;
        }
        #endregion
    }
}
