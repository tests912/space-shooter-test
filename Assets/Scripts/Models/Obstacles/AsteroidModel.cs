﻿using System;
using UniRx;
using UnityEngine;

namespace SpaceShooter.Obstacles
{
    [Serializable]
    public class AsteroidModel : IObstacleModel
    {
        #region Props
        [SerializeField] private Vector2ReactiveProperty _inGameCoords;
        public Vector2ReactiveProperty InGameCoords { get => _inGameCoords; set => _inGameCoords = value; }

        [SerializeField] private Vector2ReactiveProperty _inGameVelocity;
        public Vector2ReactiveProperty GetInGameVelocity => _inGameVelocity;

        [SerializeField] private Vector2ReactiveProperty _inGameScale;
        public Vector2ReactiveProperty GetInGameScale => _inGameScale;

        [SerializeField] private ColorReactiveProperty _inGameColor;
        public ColorReactiveProperty GetInGameColor => _inGameColor;
        #endregion

        #region Constructors
        public AsteroidModel(Vector2ReactiveProperty inGameCoords, Vector2ReactiveProperty inGameVelocity, Vector2ReactiveProperty inGameScale, ColorReactiveProperty inGameColor)
        {
            _inGameCoords = inGameCoords;
            _inGameVelocity = inGameVelocity;
            _inGameScale = inGameScale;
            _inGameColor = inGameColor;
        }
        #endregion
    }
}
