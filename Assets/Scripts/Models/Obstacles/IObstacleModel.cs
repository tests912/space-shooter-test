﻿using UniRx;

namespace SpaceShooter.Obstacles
{
    public interface IObstacleModel
    {
        #region Props
        Vector2ReactiveProperty InGameCoords { get; set; }
        Vector2ReactiveProperty GetInGameVelocity { get; }
        Vector2ReactiveProperty GetInGameScale { get; }
        ColorReactiveProperty GetInGameColor { get; }
        #endregion
    }
}