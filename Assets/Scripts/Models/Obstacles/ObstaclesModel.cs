﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;


namespace SpaceShooter.Obstacles
{
    [Serializable]
    public class ObstaclesModel
    {
        #region Fields
        [SerializeField] private IntReactiveProperty _spawnDelayMs;
        [SerializeField] private ReactiveProperty<bool> _allowSpawn;
        [SerializeField] private List<AsteroidModel> _obstacles;
        #endregion

        #region Props
        public IntReactiveProperty GetSpawnDelay => _spawnDelayMs;
        public ReactiveProperty<bool> AllowSpawn { get => _allowSpawn; set => _allowSpawn = value; }
        public List<AsteroidModel> Obstacles => _obstacles;
        #endregion

        #region Constructors
        public ObstaclesModel()
        {
            _obstacles = new List<AsteroidModel>();
            _allowSpawn = new ReactiveProperty<bool>(false);
            _spawnDelayMs = new IntReactiveProperty(350);
        }
        #endregion

        #region Methods
        public AsteroidModel CreateObstacle(Vector2 pos, float scaleFactor, float speedFactor, Color color)
        {
            var obstacle = new AsteroidModel(new Vector2ReactiveProperty(pos),
                                             new Vector2ReactiveProperty(Vector2.down * speedFactor),
                                             new Vector2ReactiveProperty(Vector2.one * scaleFactor),
                                             new ColorReactiveProperty(color));
            _obstacles.Add(obstacle);
            return obstacle;
        }
        #endregion

    }
}