﻿using Zenject;


namespace SpaceShooter.UI
{
    public class UIInstaller : MonoInstaller
    {
        #region Fields
        public UIView uIView;
        public UIModel uiModel;
        #endregion

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<UIPresenter>()
                .AsSingle()
                .WithArguments(uIView, uiModel);
        }
    }
}
