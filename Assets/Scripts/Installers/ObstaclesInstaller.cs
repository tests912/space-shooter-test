﻿using SpaceShooter.Map;
using System;
using System.IO;
using UnityEngine;
using Zenject;

namespace SpaceShooter.Obstacles
{
    public class ObstaclesInstaller : MonoInstaller
    {
        #region Fields
        [Header("Prefabs")]
        public GameObject asteroidObstaclePrefab;
        #endregion

        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ObstaclesPresenter>()
                .AsSingle()
                .NonLazy();

            Container.Bind<ObstaclesModel>()
                .AsTransient();

            Container.Bind<IObstacleModel>().To<AsteroidModel>()
                .AsTransient();

            Container.BindFactory<IObstacleModel, IMapPointModel, IObstaclePresenter, FactoryIObstaclePresenter>().To<AsteroidPresenter>()
                .FromComponentInNewPrefab(asteroidObstaclePrefab)
                .WithGameObjectName("Obstacle")
                .UnderTransformGroup("ObstaclesHolder");
        }
    }
}

