using Zenject;

namespace SpaceShooter.Game
{
    public class GameInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            // Install the main game
            InstallGame();
            InstallMisc();
        }

        #region Methods
        private void InstallGame()
        {
            Container.BindInterfacesAndSelfTo<GamePresenter>()
                .AsSingle().NonLazy();
        }

        private void InstallMisc()
        {
            Container.Bind<LevelHelper>()
                .AsSingle()
                .NonLazy();

            Container.Bind<ISerializer>().To<BinarySerializer>()
                .AsSingle()
                .NonLazy();

            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<ShipCrashSignal>();
            Container.DeclareSignal<LevelPassSignal>();
        }
        #endregion
    }
}