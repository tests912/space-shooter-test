﻿using System;
using System.IO;
using UnityEngine;
using Zenject;

namespace SpaceShooter.Map
{
    public class MapInstaller : MonoInstaller
    {
        #region Fields
        [Header("Prefabs")]
        public GameObject mapPointPrefab;
        [Space]
        public MapModel mapModel;
        public MapView mapView;
        #endregion

        public override void InstallBindings()
        {
            Container.Bind<IMapModel>().To<MapModel>()
                .AsSingle()
                .WithArguments(Path.Combine(Application.persistentDataPath, "data.save"))
                .NonLazy();

            Container.BindFactory<IMapPointModel, IMapPointPresenter, FactoryIMapPointPresenter>().To<MapPointPresenter>()
                .FromComponentInNewPrefab(mapPointPrefab)
                .WithGameObjectName("MapPoint")
                .UnderTransform(mapView.MapPointsHolder);

            Container.Bind<IMapPointPresenter>().To<MapPointPresenter>()
                .FromComponentInNewPrefab(mapPointPrefab)
                .AsTransient();

            Container.Bind<IMapPointModel>().To<MapPointModel>()
                .AsTransient()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<MapPresenter>()
                .AsSingle()
                .NonLazy();


            Container.DeclareSignal<MapSelectSignal>();
            Container.DeclareSignal<MapShowSignal>();
            Container.DeclareSignal<MapResetSignal>()
                .OptionalSubscriber();


            mapModel = (MapModel)Container.Resolve<IMapModel>();
        }
    }
}
