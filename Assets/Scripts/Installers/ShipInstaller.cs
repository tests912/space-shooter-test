﻿using SpaceShooter.Map;
using SpaceShooter.Ship.Shells;
using UnityEngine;
using Zenject;

namespace SpaceShooter.Ship
{
    public class ShipInstaller : MonoInstaller
    {
        #region Fields
        [Header("Model")]
        public ShipModel shipModel;
        [Header("Prefabs")]
        public GameObject shipPrefab;
        public GameObject bulletShellPrefab;
        #endregion

        public override void InstallBindings()
        {
            Container.Bind<IShipModel>().To<ShipModel>()
                 .FromInstance(shipModel)
                 .AsTransient()
                 .NonLazy();

            Container.Bind<IShellModel>().To<BulletModel>()
                .AsTransient().NonLazy();

            Container.BindFactory<IMapPointModel, IShipPresenter, FactoryIShipPresenter>().To<ShipPresenter>()
                .FromComponentInNewPrefab(shipPrefab)
                .WithGameObjectName("Ship");

            Container.BindFactory<IShellModel, IMapPointModel, IShellPresenter, FactoryIShellPresenter>().To<BulletPresenter>()
                .FromComponentInNewPrefab(bulletShellPrefab)
                .WithGameObjectName("Shell")
                .UnderTransformGroup("ShellsHolder");
        }
    }
}
