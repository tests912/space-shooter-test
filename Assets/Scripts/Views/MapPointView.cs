﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceShooter.Map
{
    [Serializable]
    class MapPointView
    {
        #region Fields
        public RectTransform rectTransform;
        public Image image;
        public Button button;
        public Text text;
        [Header("Sprites")]
        public Sprite missionOpen;
        public Sprite missionComleate;
        #endregion
    }
}