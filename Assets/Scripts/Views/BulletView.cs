﻿using System;
using UniRx.Triggers;
using UnityEngine;


namespace SpaceShooter.Ship.Shells
{
    [Serializable]
    class BulletView
    {
        #region Fields
        public Rigidbody2D rigidbody2d;
        public SpriteRenderer renderer;
        public ObservableCollision2DTrigger collision2DTrigger;
        #endregion
    }
}
