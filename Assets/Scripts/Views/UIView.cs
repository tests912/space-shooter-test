﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceShooter.UI
{
    [Serializable]
    public class UIView
    {
        #region Fields
        [Header("UIGroups")]
        public GameObject inGameUIGroup;
        public GameObject mapUIGroup;
        [Header("Texts")]
        public Text eventText;
        public Text livesText;
        public Text progressText;
        [Header("Buttons")]
        public Button backButton;
        public Button resetButton;
        #endregion
    }
}