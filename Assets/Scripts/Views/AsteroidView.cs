﻿using System;
using UnityEngine;
using UniRx.Triggers;


namespace SpaceShooter.Obstacles
{
    [Serializable]
    class AsteroidView
    {
        #region Fields
        public Rigidbody2D rigidbody2d;
        public SpriteRenderer renderer;
        public ObservableCollision2DTrigger collision2DTrigger;
        #endregion
    }
}