﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceShooter.Map
{
    [Serializable]
    public class MapView
    {
        #region Fields
        [Header("Holders")]
        public Transform MapPointsHolder;
        #endregion
    }
}