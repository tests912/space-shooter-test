﻿using System;
using UniRx.Triggers;
using UnityEngine;


namespace SpaceShooter.Ship
{
    [Serializable]
    class ShipView
    {
        #region Fields
        [Header("Components")]
        public SpriteRenderer renderer;
        public ObservableCollision2DTrigger collision2DTrigger;
        public BoxCollider2D collider;
        #endregion
    }
}
