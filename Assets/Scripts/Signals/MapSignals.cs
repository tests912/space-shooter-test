﻿using UnityEngine;

public class MapSelectSignal
{
    public int mapIndex;
}

public class MapResetSignal
{
}

public class MapShowSignal
{
}