﻿using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class BinarySerializer : ISerializer
{
    #region Fields
    private readonly BinaryFormatter formatter;
    #endregion

    #region Constructors
    public BinarySerializer()
    {
        formatter = new BinaryFormatter();

        SurrogateSelector ss = new SurrogateSelector();

        ss.AddSurrogate(typeof(Vector2),new StreamingContext(StreamingContextStates.All), new Vector2SerializationSurrogate());
        ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), new Vector3SerializationSurrogate());
        ss.AddSurrogate(typeof(Color), new StreamingContext(StreamingContextStates.All), new ColorSerializationSurrogate());

        formatter.SurrogateSelector = ss;
    }
    #endregion

    #region Methods
    public T Load<T>(string path)
    {
        using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
        {
            Debug.Log($"Load from {path}");
            return fs.Length > 0 ? (T)formatter.Deserialize(fs) : default;
        }
    }

    public void Save<T>(T targetObject, string path)
    {
        using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
        {
            Debug.Log($"Save to {path}");
            formatter.Serialize(fs, targetObject);
        }
    }
    #endregion
}