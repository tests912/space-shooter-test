﻿
public interface ISerializer
{
    T Load<T>(string path);

    void Save<T>(T targetObject, string path);
}

