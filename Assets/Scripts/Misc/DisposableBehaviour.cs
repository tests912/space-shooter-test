﻿using System;
using UniRx;
using UnityEngine;


public class DisposableBehaviour: MonoBehaviour
{
    #region Props
    protected CompositeDisposable GetDisposableSubscriptions { get; private set; } = new CompositeDisposable();
    protected CompositeDisposable GetAditionalDisposableSubscriptions { get; private set; } = new CompositeDisposable();
    #endregion

    #region Methods
    protected virtual void OnDestroy()
    {
        Debug.Log($"{this} dispose");
        GetDisposableSubscriptions.Dispose();
        GetAditionalDisposableSubscriptions.Dispose();
    }
    #endregion
}

public class Disposable : IDisposable
{
    #region Props
    protected CompositeDisposable GetDisposableSubscriptions { get; private set; } = new CompositeDisposable();
    protected CompositeDisposable GetAditionalDisposableSubscriptions { get; private set; } = new CompositeDisposable();
    #endregion

    #region Methods
    public virtual void Dispose()
    {
        Debug.Log($"{this} dispose");
        GetDisposableSubscriptions.Dispose();
        GetAditionalDisposableSubscriptions.Dispose();
    }
    #endregion
}
