﻿using System.Runtime.Serialization;
using UnityEngine;

sealed class Vector3SerializationSurrogate : ISerializationSurrogate
{
    #region Methods
    public void GetObjectData(System.Object obj, SerializationInfo info, StreamingContext context)
    {

        Vector3 v3 = (Vector3)obj;
        info.AddValue("x", v3.x);
        info.AddValue("y", v3.y);
        info.AddValue("z", v3.z);
    }

    public System.Object SetObjectData(System.Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
    {

        Vector3 v3 = (Vector3)obj;
        v3.x = (float)info.GetValue("x", typeof(float));
        v3.y = (float)info.GetValue("y", typeof(float));
        v3.z = (float)info.GetValue("z", typeof(float));
        obj = v3;
        return obj;
    }
    #endregion
}

sealed class Vector2SerializationSurrogate : ISerializationSurrogate
{
    #region Methods
    public void GetObjectData(System.Object obj, SerializationInfo info, StreamingContext context)
    {

        Vector2 v2 = (Vector2)obj;
        info.AddValue("x", v2.x);
        info.AddValue("y", v2.y);
    }

    public System.Object SetObjectData(System.Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
    {

        Vector2 v2 = (Vector2)obj;
        v2.x = (float)info.GetValue("x", typeof(float));
        v2.y = (float)info.GetValue("y", typeof(float));
        obj = v2;
        return obj;
    }
    #endregion
}

sealed class ColorSerializationSurrogate : ISerializationSurrogate
{
    #region Methods
    public void GetObjectData(System.Object obj, SerializationInfo info, StreamingContext context)
    {

        Color color = (Color)obj;
        info.AddValue("r", color.r);
        info.AddValue("g", color.g);
        info.AddValue("b", color.b);
        info.AddValue("a", color.a);
    }

    public System.Object SetObjectData(System.Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
    {

        Color color = (Color)obj;
        color.r = (float)info.GetValue("r", typeof(float));
        color.g = (float)info.GetValue("g", typeof(float));
        color.b = (float)info.GetValue("b", typeof(float));
        color.a = (float)info.GetValue("a", typeof(float));
        obj = color;
        return obj;
    }
    #endregion
}