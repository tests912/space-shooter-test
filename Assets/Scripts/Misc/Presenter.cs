﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Zenject;

public class Presenter<T> : DisposableBehaviour
{
    [Header("  ")]
    /// <summary>
    /// The Model of this Presenter
    /// </summary>
    [SerializeField] public T Model;
}
