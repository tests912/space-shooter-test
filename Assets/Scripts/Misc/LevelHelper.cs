﻿using UnityEngine;
using Zenject;
using UniRx;
using System;

public class LevelHelper
{
    #region Public fields
    public readonly Camera GetMainCamera;
    public IObservable<Vector2> GetMapSize;
    #endregion

    #region Private fields
    private readonly RectTransform _mainUICanvas;
    #endregion

    #region Constructors
    public LevelHelper(
        [Inject(Id = "Map")]
        RectTransform mainUICanvas,
        [Inject(Id = "Main")]
        Camera camera)
    {
        GetMainCamera = camera;
        _mainUICanvas = mainUICanvas;
        GetMapSize = _mainUICanvas.ObserveEveryValueChanged(size => size.sizeDelta);
    }
    #endregion

    #region Methods
    public Vector3 GetWorldScreen()
    {
        return GetMainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height));
    }

    public (bool isOut, Vector3 mirrorPos) CheckForOutOfScreen(Vector3 currentPos, Vector2 velocity, float scale = 1)
    {
        bool isOut = false;
        Vector3 mirrorPos = currentPos;
        Vector3 worldScreen = GetWorldScreen();

        if (currentPos.y - scale > worldScreen.y && IsMovingInDirection(Vector3.up, velocity))
        {
            isOut = true;
            mirrorPos.y = worldScreen.y + scale;
        }
        if (currentPos.y + scale < -worldScreen.y && IsMovingInDirection(Vector3.down, velocity))
        {
            isOut = true;
            mirrorPos.y = -worldScreen.y - scale;
        }
        if (currentPos.x - scale > worldScreen.x )
        {
            isOut = true;
            mirrorPos.x = -worldScreen.x - scale;
        }
        if (currentPos.x + scale < -worldScreen.x )
        {
            isOut = true;
            mirrorPos.x = worldScreen.x + scale;
        }

        return (isOut, mirrorPos);
    }

    public bool IsMovingInDirection(Vector3 dir, Vector2 velocity)
    {
        return Vector3.Dot(dir, velocity) > 0;
    }
    #endregion
}