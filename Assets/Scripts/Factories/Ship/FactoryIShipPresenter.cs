﻿using SpaceShooter.Map;
using Zenject;

namespace SpaceShooter.Ship
{
    public class FactoryIShipPresenter : PlaceholderFactory<IMapPointModel, IShipPresenter>
    { }
}