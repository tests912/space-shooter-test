﻿using SpaceShooter.Map;
using SpaceShooter.Ship.Shells;
using Zenject;

namespace SpaceShooter.Ship
{
    public class FactoryIShellPresenter : PlaceholderFactory<IShellModel, IMapPointModel, IShellPresenter>
    { }
}
