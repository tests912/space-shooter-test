﻿using SpaceShooter.Map;
using Zenject;

namespace SpaceShooter.Obstacles
{
    public class FactoryIObstaclePresenter : PlaceholderFactory<IObstacleModel, IMapPointModel, IObstaclePresenter>
    { }
}